Feature: Connexion

  Scenario: Tentative de connexion avec les bons identifiants
    Given Je suis sur la page de connexion
    Then Je devrais voir un formulaire de connexion
    When Je saisis les bons identifiants
    Then Je devrais être connecté
    And Je devrais être redirigé vers la liste des partitions

  Scenario: Tentative de connexion avec les mauvais identifiants
    Given Je suis sur la page de connexion
    Then Je devrais voir un formulaire de connexion
    When Je saisis les mauvais identifiants
    Then Je devrais être redirigé vers la page de connexion

  Scenario: Tentative d’accès à une page qui requiert une authentification
    Given Je suis un utilisateur non connecté
    When Je me rends sur la page qui liste les partitions
    Then Je devrais être redirigé vers la page d’accueil
