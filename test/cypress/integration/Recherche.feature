Feature: Recherche

  Scenario: Recherche par mots-clefs en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je fais une recherche par mots-clefs
    Then Si tout se passe bien, je devrais avoir des résultats

  Scenario: Recherche par mots-clefs en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de faire une recherche par mots-clefs
    Then Je devrais être redirigé vers la page de connexion

  Scenario: Recherche avancée en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je fais une recherche avancée
    Then Si tout se passe bien, je devrais avoir des résultats
