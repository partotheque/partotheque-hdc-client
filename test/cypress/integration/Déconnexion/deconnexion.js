/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis sur une page à accès restreint`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

When(`Je me déconnecte`, () => {
  cy.route({
    method: 'DELETE',
    url: '/users/sign_out',
    status: 204,
    response: [],
  }).as('signOut');

  cy.get('.t-disconnect').click();
  cy.wait('@signOut');
});

Then(`Je suis redirigé vers la page d’accueil`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});

And(`Je suis bien déconnecté`, () => {
  cy.getLocalStorageItem('xsrf').should('not.exist');
  cy.get('.t-disconnect').should('not.exist');
});

And(`Je dois renseigner de nouveau mes identifiants pour pouvoir accéder à la page précédente`, () => {
  cy.route({
    method: 'GET',
    url: '/scores',
    status: 401,
    response: [],
  }).as('unauthorizedListScores');

  cy.visit('/scores');
  cy.wait('@unauthorizedListScores');

  cy.url().should('eql', 'http://localhost:8080/');
});
