/* eslint-disable quotes */

import {
  And, Given, Then,
} from 'cypress-cucumber-preprocessor/steps';

Given(`La page est ouverte dans un navigateur`, () => {
  cy.visit('/');
});

Then(`Je devrais voir la page d'accueil`, () => {
  cy.get('#q-app').should('exist');
});

And(`qui devrait contenir le formulaire de connexion`, () => {
  cy.get('.t-signin-form')
    .should('exist')
    .and('be.visible');
});

And(`une barre de titre`, () => {
  cy.get('.q-toolbar__title').should('not.be.empty');
});

And(`cette barre doit contenir le bon titre`, () => {
  cy.get('.q-toolbar__title').should('contain', 'Partothèque');
});
