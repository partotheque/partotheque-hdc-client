Feature: Déconnexion

  Scenario: Déconnexion quand tout se passe bien
    Given Je suis sur une page à accès restreint
    When Je me déconnecte
    Then Je suis redirigé vers la page d’accueil
    And Je suis bien déconnecté
    And Je dois renseigner de nouveau mes identifiants pour pouvoir accéder à la page précédente
