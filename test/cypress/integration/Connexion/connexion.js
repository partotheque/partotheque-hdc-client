/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis sur la page de connexion`, () => {
  cy.visit('/');
});

Given(`Je suis un utilisateur non connecté`, () => {});

Then(`Je devrais voir un formulaire de connexion`, () => {
  cy.get('.t-signin-form')
    .should('exist')
    .and('be.visible');
});

When(`Je saisis les bons identifiants`, () => {
  cy.server();
  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
});

Then(`Je devrais être connecté`, () => {
  cy.getLocalStorageItem('xsrf').should('exist');
  cy.get('.t-disconnect').should('exist');
});

And(`Je devrais être redirigé vers la liste des partitions`, () => {
  cy.route({
    method: 'GET',
    url: 'scores',
    status: 200,
  });

  cy.url().should('match', /scores/);
});

When(`Je saisis les mauvais identifiants`, () => {
  cy.server();
  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 401,
    response: [],
  }).as('signin');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('ppassword');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.route({
    method: 'GET',
    url: 'scores',
    status: 401,
  });

  cy.url().should('not.match', /scores/);
});

When(`Je me rends sur la page qui liste les partitions`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/scores',
    status: 401,
    response: {
      redirect: '/',
    },
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');
});

Then(`Je devrais être redirigé vers la page d’accueil`, () => {
  cy.url().should('not.match', /scores/);
});
