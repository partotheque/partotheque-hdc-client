/* eslint-disable quotes */

import {
  Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

When(`Je fais une recherche avancée`, () => {
  const search = 'coucou';

  cy.route({
    method: 'GET',
    url: '/search',
  }).as('search');

  cy.route({
    method: 'POST',
    url: 'scores/search',
    response: 'fixture:search/precise.json',
  }).as('results');

  cy.get('[href="/search"]').click();
  cy.get('[data-cy="search-tab-advanced"]').click();

  cy.get('[data-cy="search-field-title"]').type(search);
  cy.get('[data-cy="advanced-search-form"]').submit();

  cy.wait('@results');
});

Then(`Si tout se passe bien, je devrais avoir des résultats`, () => {
  cy.get('[data-cy="results"]').should('exist');
});
