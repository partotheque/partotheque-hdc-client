/* eslint-disable quotes */

import {
  Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je fais une recherche par mots-clefs`, () => {
  const search = 'bisous';

  cy.route({
    method: 'GET',
    url: '/search',
  }).as('search');

  cy.route({
    method: 'POST',
    url: `/multi_search?search=${search}`,
    response: 'fixture:search/multi_search.json',
  }).as('results');

  cy.get('[href="/search"]').click();

  cy.get('[data-cy="search-field"]').type(search);
  cy.get('[data-cy="keywords-search-form"]').submit();

  cy.wait('@results');
});

When(`Je tente de faire une recherche par mots-clefs`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/search',
    status: 401,
    response: [],
  });

  cy.visit('/search');
});

Then(`Si tout se passe bien, je devrais avoir des résultats`, () => {
  cy.get('[data-cy="results"]').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
