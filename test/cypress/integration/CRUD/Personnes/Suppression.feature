Feature: Suppression d’une personne

  Scenario: Suppression d’une personne en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les personnes
    When J’en choisis une pour la supprimer
    Then Je ne devrais plus la voir dans la liste
