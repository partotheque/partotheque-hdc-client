/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les personnes et j’en choisis une pour avoir davantage de détails`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('listPeople');

  cy.visit('/people');
  cy.wait('@listPeople');

  cy.route({
    method: 'GET',
    url: '/people/1',
    response: 'fixture:people/one.json',
  });
  cy.get('[data-cy=show-people-link]:first').click();
});

When(`Je tente d’en savoir plus sur une personne`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/people/1',
    status: 401,
    response: [],
  }).as('getPeople');

  cy.visit('/people/1');
  cy.wait('@getPeople');
});

Then(`Je devrais les avoir`, () => {
  cy.get('[data-cy=people-show] > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)').should('not.be', 'empty');
});

And(`Ainsi que ses partitions composées et arrangées s’il y en a`, () => {
  cy.get('[data-cy=scores-people]').should('not.be', 'empty');
});

And(`Chacune de ses partitions doit avoir un lien sur son élément constituant amenant à sa page de détails`, () => {
  cy.get('[data-cy=scores-composed-by-people-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/scores\/\d/);
  });

  cy.get('[data-cy=scores-arranged-by-people-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/scores\/\d/);
  });
});

And(`Je devrais également voir les concerts qu’elle a dirigés`, () => {
  cy.get('[data-cy=concerts-people]').should('not.be', 'empty');
});

And(`Un lien sur leur élément constituant`, () => {
  cy.get('[data-cy=concerts-directed-by-people-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/concerts\/\d/);
  });
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('not.match', /people/);
});
