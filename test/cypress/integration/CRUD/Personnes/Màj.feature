Feature: Mise à jour d’une personne

  Scenario: Mise à jour d’une personne en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant de mettre à jour une personne
    When Je remplis le formulaire correctement
    Then Cette personne devrait être mise à jour
    And Je devrais être redirigé vers la liste des personnes
    And Je devrais voir que cette personne a été mise à jour

  Scenario: Mise à jour d’une personne en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant de mettre à jour une personne
    Then Je devrais être redirigé vers la page de connexion
