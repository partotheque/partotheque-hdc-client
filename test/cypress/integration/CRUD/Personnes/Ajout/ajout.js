/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant d’ajouter une personne`, () => {
  cy.route({
    method: 'GET',
    url: '/people/add',
    response: [],
  }).as('addPeople');

  cy.get('.t-add').click();
  cy.get('.t-add-people').click();
});

When(`Je tente de me rendre sur la page permettant d’ajouter une personne`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/people/add',
    status: 401,
    response: [],
  }).as('addPeople');

  cy.visit('/people/add');
});

When(`Je remplis le formulaire correctement`, () => {
  cy.route({
    method: 'POST',
    url: '/people',
    status: 201,
    response: [],
  }).as('createdPeople');

  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all_plus_one.json',
  }).as('listPeople');

  cy.get('.t-add-people-last-name').type('Test');
  cy.get('.t-add-people-form').submit();

  cy.wait('@listPeople');
});

Then(`Cette personne devrait être ajoutée à la base de données`, () => {
  cy.wait('@createdPeople').should('have.property', 'status', 201);
});

And(`Je devrais être redirigé vers la liste des personnes`, () => {
  cy.url().should('eql', 'http://localhost:8080/people');
});

And(`Je devrais voir cette personne dans la liste`, () => {
  cy.contains('Test').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
