/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les acteurs`, () => {
  cy.server();
});

Then(`Je devrais les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('listPeople');

  cy.visit('/people');
  cy.wait('@listPeople');

  cy.get('[data-cy=row]').should('have.length', 5);
});

And(`Chacun de ces acteurs devrait avoir un lien sur son élément constituant amenant à sa page de détails`, () => {
  cy.get('[data-cy=show-people-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/people\/\d/);
  });
});

And(`Je devrais pouvoir les filtrer`, () => {
  cy.get('[data-cy=filter-people]').type('doe');

  cy.get('[data-cy=row]').should('have.length', 2);
});

Then(`Je ne devrais pas les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: [],
  }).as('listPeople');

  cy.visit('/people');
  cy.wait('@listPeople');

  cy.get('[data-cy=row]').should('have.length', 0);
});

And(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    status: 401,
    response: [],
  }).as('listPeople');

  cy.visit('/people');
  cy.wait('@listPeople');

  cy.url().should('be.eql', 'http://localhost:8080/');
});
