/* eslint-disable quotes */

import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

When(`Je me rends sur la page qui liste les personnes`, () => {
  cy.server();

  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('listPeople');

  cy.visit('/people');
  cy.wait('@listPeople');
});

When(`J’en choisis une pour la supprimer`, () => {
  cy.route({
    method: 'DELETE',
    url: '/people/1',
    status: 204,
    response: [],
  }).as('deletePeople');

  cy.get('.t-delete:first').click();
  cy.wait('@deletePeople');
});

Then(`Je ne devrais plus la voir dans la liste`, () => {
  cy.get('[data-cy=row]').should('have.length', 4);
});
