/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant de mettre à jour une personne`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('listPeople');

  cy.get('.t-people').click();
  cy.wait('@listPeople');
  cy.get('[data-cy=edit]:first').click();
});

When(`Je tente de me rendre sur la page permettant de mettre à jour une personne`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/people/1/update',
    status: 401,
    response: [],
  });

  cy.visit('/people/1/update');
});

When(`Je remplis le formulaire correctement`, () => {
  cy.route({
    method: 'PUT',
    url: '/people/1',
    status: 200,
    response: [],
  }).as('updatedPeople');

  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all_with_one_updated.json',
  }).as('listUpdatedPeople');

  cy.get('[type=textarea]').type('Compositrice tombée dans l’oubli…');
  cy.get('.t-update-people-form').submit();
});

Then(`Cette personne devrait être mise à jour`, () => {
  cy.wait('@updatedPeople').should('have.property', 'status', 200);
});

And(`Je devrais être redirigé vers la liste des personnes`, () => {
  cy.url().should('eql', 'http://localhost:8080/people');
});

And(`Je devrais voir que cette personne a été mise à jour`, () => {
  cy.wait('@listUpdatedPeople');
  cy.contains('Compositrice').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
