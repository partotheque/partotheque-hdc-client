Feature: Ajout d’une personne

  Scenario: Ajout d’une personne en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une personne
    When Je remplis le formulaire correctement
    Then Cette personne devrait être ajoutée à la base de données
    And Je devrais être redirigé vers la liste des personnes
    And Je devrais voir cette personne dans la liste

  Scenario: Ajout d’une personne en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant d’ajouter une personne
    Then Je devrais être redirigé vers la page de connexion
