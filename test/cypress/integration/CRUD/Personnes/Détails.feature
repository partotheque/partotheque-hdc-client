Feature: Détails d’une personne

  Scenario: Détails d’une personne en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les personnes et j’en choisis une pour avoir davantage de détails
    Then Je devrais les avoir
    And Ainsi que ses partitions composées et arrangées s’il y en a
    And Chacune de ses partitions doit avoir un lien sur son élément constituant amenant à sa page de détails
    And Je devrais également voir les concerts qu’elle a dirigés
    And Un lien sur leur élément constituant

  Scenario: Détails d’une personne en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente d’en savoir plus sur une personne
    Then Je devrais être redirigé vers la page de connexion
