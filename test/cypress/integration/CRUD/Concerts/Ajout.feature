Feature: Ajout d’un concert

  Scenario: Ajout d’un concert en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter un concert
    When Je donne un nom au concert
    And Je lui assigne un chef existant
    And Je crée en même temps un nouveau chef
    And Je valide le formulaire
    Then Ce concert devrait être ajouté à la base de données
    And Je devrais être redirigé vers la liste des concerts
    And Je devrais voir ce concert dans la liste
    And Il devrait être associé aux bons chefs

  Scenario: Ajout d’un concert en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant d’ajouter un concert
    Then Je devrais être redirigé vers la page de connexion
