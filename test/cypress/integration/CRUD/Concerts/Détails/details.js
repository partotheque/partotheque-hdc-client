/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les concerts et j’en choisis un pour avoir davantage de détails`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/concerts',
    response: 'fixture:concerts/all.json',
  }).as('listConcerts');

  cy.visit('/concerts');
  cy.wait('@listConcerts');

  cy.route({
    method: 'GET',
    url: '/concerts/1',
    response: 'fixture:concerts/one.json',
  });
  cy.get('[data-cy=show-concert-link]:first').click();
});

When(`Je tente d’en savoir plus sur un concert`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/concerts/1',
    status: 401,
    response: [],
  }).as('getConcert');

  cy.visit('/concerts/1');
  cy.wait('@getConcert');
});

Then(`Je devrais les avoir`, () => {
  cy.get('[data-cy=concert-show-name]').should('not.be', 'empty');
});

And(`Ainsi que la liste des partitions interprétées à l’occasion de ce concert`, () => {
  cy.get('[data-cy=scores-concert]').should('exist');
});

And(`Chacune de ces partitions doit avoir un lien sur son élément constituant amenant à sa page de détails`, () => {
  cy.get('[data-cy=score-concert-link]').each(($el, index) => {
    expect($el).to.have.attr('href', `/scores/${index + 1}`);
  });
});

And(`Chaque chef de chaque concert devrait avoir le même type de lien`, () => {
  cy.get('[data-cy=conductor-concert-link]').each(($el, index) => {
    expect($el).to.have.attr('href', `/people/${index + 1}`);
  });
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('not.match', /score/);
});
