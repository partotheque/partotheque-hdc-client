/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les concerts`, () => {
  cy.server();
});

Then(`Je devrais les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts',
    response: 'fixture:concerts/all.json',
  }).as('listConcerts');

  cy.visit('/concerts');
  cy.wait('@listConcerts');

  cy.get('[data-cy=row]').should('have.length', 3);
});

And(`Chacun de ces concerts devrait avoir un lien sur son élément constituant amenant à sa page de détails`, () => {
  cy.get('[data-cy=show-concert-link]').each(($el, index) => {
    expect($el).to.have.attr('href', `/concerts/${index + 1}`);
  });
});

And(`Idem pour les chefs`, () => {
  cy.get('[data-cy=show-conductor-concert-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/people\/\d/);
  });
});

And(`Je devrais pouvoir les filtrer`, () => {
  cy.get('[data-cy=filter-concerts]').type('talaud');

  cy.get('[data-cy=row]').should('have.length', 2);
});

Then(`Je ne devrais pas les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts',
    response: [],
  }).as('listConcerts');

  cy.visit('/concerts');
  cy.wait('@listConcerts');

  cy.get('[data-cy=row]').should('have.length', 0);
});

And(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts',
    status: 401,
    response: [],
  }).as('listConcerts');

  cy.visit('/concerts');
  cy.wait('@listConcerts');

  cy.url().should('be.eql', 'http://localhost:8080/');
});
