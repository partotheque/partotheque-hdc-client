/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant de mettre à jour un concert`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts',
    response: 'fixture:concerts/all.json',
  }).as('listConcerts');

  cy.get('[href="/concerts"]').click();
  cy.wait('@listConcerts');

  cy.get('[data-cy=edit]:first').click();
});

When(`Je tente de me rendre sur la page permettant de mettre à jour un concert`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/concerts/1/update',
    status: 401,
    response: [],
  });

  cy.visit('/concerts/1/update');
});

When(`Je remplis le formulaire correctement`, () => {
  cy.route({
    method: 'PUT',
    url: '/concerts/1',
    status: 200,
    response: [],
  }).as('updatedConcert');

  cy.route({
    method: 'GET',
    url: '/concerts',
    status: 200,
    response: 'fixture:concerts/all_with_one_updated.json',
  }).as('listUpdatedConcert');

  cy.get('[aria-label="Participant"]').type('Coucou les gens');
  cy.get('.q-form').submit();
});

Then(`Ce concert devrait être mis à jour`, () => {
  cy.wait('@updatedConcert').should('have.property', 'status', 200);
});

And(`Je devrais être redirigé vers la liste des concerts`, () => {
  cy.url().should('eql', 'http://localhost:8080/concerts');
});

And(`Je devrais voir que ce concert a été mis à jour`, () => {
  cy.wait('@listUpdatedConcert');
  cy.contains('Coucou les gens').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
