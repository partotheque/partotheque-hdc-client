Feature: Liste des concerts

  Scenario: Affichage des concerts quand le serveur répond correctement
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les concerts
    Then Je devrais les voir
    And Chacun de ces concerts devrait avoir un lien sur son élément constituant amenant à sa page de détails
    And Idem pour les chefs
    And Je devrais pouvoir les filtrer

  Scenario: Affichage des concerts pour un utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je me rends sur la page qui liste les concerts
    Then Je ne devrais pas les voir
    And Je devrais être redirigé vers la page de connexion

  Scenario: Affichage des concerts quand le serveur ne répond pas correctement
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les concerts
    Then Je ne devrais pas les voir
