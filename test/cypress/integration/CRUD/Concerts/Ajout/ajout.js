/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant d’ajouter un concert`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts/add',
    response: [],
  }).as('addConcert');

  cy.get('.t-add').click();
  cy.get('.t-add-concert').click();
});

When(`Je tente de me rendre sur la page permettant d’ajouter un concert`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/concerts/add',
    status: 401,
    response: [],
  }).as('addConcert');

  cy.visit('/concerts/add');
});

When(`Je donne un nom au concert`, () => {
  cy.route({
    method: 'POST',
    url: '/concerts',
    status: 201,
    response: [],
  }).as('createdConcert');

  cy.get('.t-add-concert-name').type('Test');
});

And(`Je lui assigne un chef existant`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('people');

  cy.get('.t-add-conductor').click();
  cy.get('.t-people-list').type('doe');
  cy.get('div.q-virtual-scroll__content').as('composersList');
  cy.contains('Jane Doe').click();
});

And(`Je crée en même temps un nouveau chef`, () => {
  cy.get('.t-add-conductor').click();
  cy.get(':nth-child(8) > :nth-child(2) > .q-list > :nth-child(2) > .q-item__section--avatar > .q-radio').click();
  cy.get('[aria-label="Nom de famille"]').type('Doe');
  cy.get('[aria-label="Prénom"]').type('Elisabeth');
});

And(`Je valide le formulaire`, () => {
  cy.route({
    method: 'GET',
    url: '/concerts',
    response: 'fixture:concerts/all_plus_one.json',
  }).as('listConcerts');

  cy.get('.t-add-concert-form').submit();
  cy.wait('@listConcerts');
});

Then(`Ce concert devrait être ajouté à la base de données`, () => {
  cy.wait('@createdConcert').should('have.property', 'status', 201);
});

And(`Je devrais être redirigé vers la liste des concerts`, () => {
  cy.url().should('eql', 'http://localhost:8080/concerts');
});

And(`Je devrais voir ce concert dans la liste`, () => {
  cy.contains('Test').should('exist');
});

And(`Il devrait être associé aux bons chefs`, () => {
  cy.contains('Jane').should('exist');
  cy.contains('Elisabeth').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
