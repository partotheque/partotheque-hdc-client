/* eslint-disable quotes */

import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

When(`Je me rends sur la page qui liste les concerts`, () => {
  cy.server();

  cy.route({
    method: 'GET',
    url: '/concerts',
    response: 'fixture:concerts/all.json',
  }).as('listConcerts');

  cy.visit('/concerts');
  cy.wait('@listConcerts');
});

When(`J’en choisis un pour le supprimer`, () => {
  cy.route({
    method: 'DELETE',
    url: '/concerts/1',
    status: 204,
    response: [],
  }).as('deleteConcert');

  cy.get('.t-delete:first').click();
  cy.wait('@deleteConcert');
});

Then(`Je ne devrais plus le voir dans la liste`, () => {
  cy.get('[data-cy=row]').should('have.length', 2);
});
