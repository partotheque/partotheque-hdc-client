Feature: Détails d’un concert

  Scenario: Détails d’un concert en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les concerts et j’en choisis un pour avoir davantage de détails
    Then Je devrais les avoir
    And Ainsi que la liste des partitions interprétées à l’occasion de ce concert
    And Chacune de ces partitions doit avoir un lien sur son élément constituant amenant à sa page de détails
    And Chaque chef de chaque concert devrait avoir le même type de lien

  Scenario: Détails d’un concert en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente d’en savoir plus sur un concert
    Then Je devrais être redirigé vers la page de connexion
