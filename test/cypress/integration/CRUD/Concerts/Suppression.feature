Feature: Suppression d’un concert

  Scenario: Suppression d’un concert en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les concerts
    When J’en choisis un pour le supprimer
    Then Je ne devrais plus le voir dans la liste
