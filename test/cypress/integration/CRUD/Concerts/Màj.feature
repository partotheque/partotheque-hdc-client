Feature: Mise à jour d’un concert

  Scenario: En tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant de mettre à jour un concert
    When Je remplis le formulaire correctement
    Then Ce concert devrait être mis à jour
    And Je devrais être redirigé vers la liste des concerts
    And Je devrais voir que ce concert a été mis à jour

  Scenario: En tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant de mettre à jour un concert
    Then Je devrais être redirigé vers la page de connexion
