Feature: Ajout d’une partition

  Scenario: Ajout d’une partition en tant qu’utilisateur connecté - Champs requis remplis
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    When Je remplis le formulaire correctement
    Then Cette partition devrait être ajoutée à la base de données
    And Je devrais être redirigé vers la liste des partitions
    And Je devrais voir cette partition dans la liste

  Scenario: Ajout d’une partition en tant qu’utilisateur connecté - Champ titre non rempli
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    When J’oublie de saisir le titre
    Then Je ne devrais pas pouvoir soumettre le formulaire
    And Je devrais voir un message d’erreur sous le champ "titre"

  Scenario: Ajout d’une partition en tant qu’utilisateur connecté - Champ numéro non rempli
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    When J’oublie de saisir le numéro
    Then Je ne devrais pas pouvoir soumettre le formulaire
    And Je devrais voir un message d’erreur sous le champ "numéro"

  Scenario: Ajout d’une partition en tant qu’utilisateur connecté - Champ réf archive non rempli
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    When J’oublie de saisir la référence archive
    Then Je ne devrais pas pouvoir soumettre le formulaire
    And Je devrais voir un message d’erreur sous le champ "référence archive"

  Scenario: Ajout d’une partition avec compositeur et arrangeur existants en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    And Je lui assigne un compositeur et un arrangeur déjà existants
    Then Cette partition devrait être ajoutée à la base de données
    And Je devrais être redirigé vers la liste des partitions
    And Je devrais voir cette partition dans la liste
    And Son compositeur et son arrangeur doivent être les bons

  Scenario: Ajout d’une partition avec compositeur et arrangeur non existants en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant d’ajouter une partition
    And Je lui assigne un compositeur et un arrangeur non existants
    Then Cette partition devrait être ajoutée à la base de données
    And Je devrais être redirigé vers la liste des partitions
    And Je devrais voir cette partition dans la liste
    And Son compositeur et son arrangeur doivent être les bons

  Scenario: Ajout d’une partition en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant d’ajouter une partition
    Then Je devrais être redirigé vers la page de connexion
