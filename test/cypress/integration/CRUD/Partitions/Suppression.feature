Feature: Suppression d’une partition

  Scenario: Suppression d’une partition en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les partitions
    When J’en choisis une pour la supprimer
    Then Je ne devrais plus la voir dans la liste
