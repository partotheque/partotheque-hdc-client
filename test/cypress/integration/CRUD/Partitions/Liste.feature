Feature: Liste des partitions

  Scenario: Affichage des partitions quand le serveur répond correctement
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les partitions
    Then Je devrais les voir
    And L’élément constituant de chaque partition devrait être un lien m’amenant sur la page de détails de la partition
    And Chaque compositeur devrait avoir le même type de lien
    And Idem pour les arrangeurs
    And Je devrais pouvoir les filtrer

  Scenario: Affichage des partitions pour un utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je me rends sur la page qui liste les partitions
    Then Je ne devrais pas les voir
    And Je devrais être redirigé vers la page de connexion

  Scenario: Affichage des partitions quand le serveur ne répond pas correctement
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les partitions
    Then Je ne devrais pas les voir
