/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les partitions`, () => {
  cy.server();
});

Then(`Je devrais les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');

  cy.get('[data-cy=row]').should('have.length', 3);
});

And(`L’élément constituant de chaque partition devrait être un lien m’amenant sur la page de détails de la partition`, () => {
  cy.get('[data-cy=show-score-link]:first').should('have.attr', 'href', '/scores/1');
});

And(`Chaque compositeur devrait avoir le même type de lien`, () => {
  cy.get('[data-cy=composer-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/people\/\d/);
  });
});

And(`Idem pour les arrangeurs`, () => {
  cy.get('[data-cy=arranger-link]').each(($el) => {
    expect($el).to.have.attr('href').and.match(/\/people\/\d/);
  });
});

And(`Je devrais pouvoir les filtrer`, () => {
  cy.get('[data-cy=filter-scores]').type('coucou');

  cy.get('[data-cy=row]').should('have.length', 1);
});

Then(`Je ne devrais pas les voir`, () => {
  cy.route({
    method: 'GET',
    url: '/scores',
    response: [],
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');

  cy.get('[data-cy=row]').should('have.length', 0);
});

And(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.route({
    method: 'GET',
    url: '/scores',
    status: 401,
    response: [],
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');

  cy.url().should('be.eql', 'http://localhost:8080/');
});
