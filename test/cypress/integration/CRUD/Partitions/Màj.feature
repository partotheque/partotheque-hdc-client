Feature: Mise à jour d’une partition

  Scenario: En tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page permettant de mettre à jour une partition
    When Je remplis le formulaire correctement
    Then Cette partition devrait être mise à jour
    And Je devrais être redirigé vers la liste des partitions
    And Je devrais voir que cette partition a été mise à jour

  Scenario: En tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente de me rendre sur la page permettant de mettre à jour une partition
    Then Je devrais être redirigé vers la page de connexion
