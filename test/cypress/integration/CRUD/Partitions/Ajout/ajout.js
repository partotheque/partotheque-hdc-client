/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant d’ajouter une partition`, () => {
  cy.route({
    method: 'GET',
    url: '/scores/add',
    response: [],
  }).as('addScore');

  cy.get('.t-add').click();
  cy.get('.t-add-score').click();
});

When(`Je tente de me rendre sur la page permettant d’ajouter une partition`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/scores/add',
    status: 401,
    response: [],
  }).as('addScore');

  cy.visit('/scores/add');
});

When(`Je remplis le formulaire correctement`, () => {
  cy.route({
    method: 'POST',
    url: '/scores',
    status: 201,
    response: [],
  }).as('createdScore');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all_plus_one.json',
  }).as('listScores');

  cy.get('.t-add-score-title').type('Test');
  cy.get('.t-add-score-number').type('1');
  cy.get('.t-add-score-archive-ref').type('Test - 1');
  cy.get('.t-add-score-form').submit();

  cy.wait('@listScores');
});

When(`J’oublie de saisir le titre`, () => {
  cy.get('.t-add-score-archive-ref').type('Test - 1');
  cy.get('.t-add-score-form').submit();
});

When(`J’oublie de saisir le numéro`, () => {
  cy.get('.t-add-score-title').type('Test');
  cy.get('.t-add-score-archive-ref').type('Test - 1');
  cy.get('.t-add-score-form').submit();
});

When(`J’oublie de saisir la référence archive`, () => {
  cy.get('.t-add-score-title').type('Test');
  cy.get('.t-add-score-number').type('1');
  cy.get('.t-add-score-form').submit();
});

Then(`Cette partition devrait être ajoutée à la base de données`, () => {
  cy.wait('@createdScore').should('have.property', 'status', 201);
});

Then(`Je ne devrais pas pouvoir soumettre le formulaire`, () => {
  cy.url().should('eql', 'http://localhost:8080/scores/add');
});

And(`Je devrais être redirigé vers la liste des partitions`, () => {
  cy.url().should('eql', 'http://localhost:8080/scores');
});

And(`Je devrais voir cette partition dans la liste`, () => {
  cy.contains('Test').should('exist');
});

And(`Je devrais voir un message d’erreur sous le champ {string}`, () => {
  cy.get('i.text-negative').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});

And(`Je lui assigne un compositeur et un arrangeur déjà existants`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('people');

  cy.route({
    method: 'POST',
    url: '/scores',
    status: 201,
    response: [],
  }).as('createdScore');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all_plus_one.json',
  }).as('listScores');

  cy.get('.t-add-score-title').type('Test');
  cy.get('.t-add-score-number').type('1');
  cy.get('.t-add-score-archive-ref').type('Test - 1');

  cy.get('.t-composers-list').type('doe');
  cy.get('div.q-virtual-scroll__content').as('composersList');
  cy.get('@composersList').children().should('have.length', 2);
  cy.contains('Jane Doe').click();

  cy.get('.t-arrangers-list').type('part');
  cy.get('div.q-virtual-scroll__content').as('arrangersList');
  cy.get('@arrangersList').children().should('have.length', 1);
  cy.contains('Léo Part').click();

  cy.get('.t-add-score-form').submit();
});

And(`Je lui assigne un compositeur et un arrangeur non existants`, () => {
  cy.route({
    method: 'GET',
    url: '/people',
    response: 'fixture:people/all.json',
  }).as('people');

  cy.route({
    method: 'POST',
    url: '/scores',
    status: 201,
    response: [],
  }).as('createdScore');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all_plus_one.json',
  }).as('listScores');

  cy.get('.t-add-score-title').type('Test');
  cy.get('.t-add-score-number').type('1');
  cy.get('.t-add-score-archive-ref').type('Test - 1');

  cy.get('.q-form > div:nth-child(18) > div:nth-child(2) > label:nth-child(2)').click();

  cy.get('[aria-label="Nom de famille"]').type('Doe');
  cy.get('[aria-label="Prénom"]').type('Jane');

  cy.get('.q-form > div:nth-child(19) > div:nth-child(2) > label:nth-child(2)').click();
  cy.get('[aria-label="Nom de famille"]').last().type('Part');
  cy.get('[aria-label="Prénom"]').last().type('Léo');
  cy.get('.t-add-score-form').submit();
});

And(`Son compositeur et son arrangeur doivent être les bons`, () => {
  cy.get(':nth-child(4) > [data-cy=composer] > a').should('include.text', 'Jane Doe');
  cy.get(':nth-child(4) > [data-cy=arranger] > a').should('include.text', 'Léo Part');
});
