/* eslint-disable quotes */

import {
  Given, Then, When, And,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.server();

  cy.route({
    method: 'POST',
    url: '/users/sign_in',
    status: 200,
    headers: {
      'X-Xsrf-Token': 'xxx-xxx',
    },
    response: [],
  }).as('signin');

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/');

  cy.get('.t-signin-email').type('j@d.oe');
  cy.get('.t-signin-password').type('password');
  cy.get('.t-signin-form').submit();

  cy.wait('@signin');
  cy.wait('@listScores');
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page permettant de mettre à jour une partition`, () => {
  cy.get('[data-cy=edit]:first').click();
});

When(`Je tente de me rendre sur la page permettant de mettre à jour une partition`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/scores/1/update',
    status: 401,
    response: [],
  });

  cy.visit('/scores/1/update');
});

When(`Je remplis le formulaire correctement`, () => {
  cy.route({
    method: 'PUT',
    url: '/scores/1',
    status: 200,
    response: [],
  }).as('updatedScore');

  cy.route({
    method: 'GET',
    url: '/scores',
    status: 200,
    response: 'fixture:scores/all_with_one_updated.json',
  }).as('listUpdatedScore');

  cy.get('[data-cy=update-score-title]').clear().type('Coucou les gens');
  cy.get('.t-update-score-form').submit();
});

Then(`Cette partition devrait être mise à jour`, () => {
  cy.wait('@updatedScore').should('have.property', 'status', 200);
});

And(`Je devrais être redirigé vers la liste des partitions`, () => {
  cy.url().should('eql', 'http://localhost:8080/scores');
});

And(`Je devrais voir que cette partition a été mise à jour`, () => {
  cy.wait('@listUpdatedScore');
  cy.contains('Coucou les gens').should('exist');
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('eql', 'http://localhost:8080/');
});
