/* eslint-disable quotes */

import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

When(`Je me rends sur la page qui liste les partitions`, () => {
  cy.server();

  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');
});

When(`J’en choisis une pour la supprimer`, () => {
  cy.route({
    method: 'DELETE',
    url: '/scores/1',
    status: 204,
    response: [],
  }).as('deleteScore');

  cy.get('.t-delete:first').click();
  cy.wait('@deleteScore');
});

Then(`Je ne devrais plus la voir dans la liste`, () => {
  cy.get('[data-cy=row]').should('have.length', 2);
});
