Feature: Détails d’une partition

  Scenario: Détails d’une partition en tant qu’utilisateur connecté
    Given Je suis un utilisateur connecté
    When Je me rends sur la page qui liste les partitions et j’en choisis une pour avoir davantage de détails
    Then Je devrais les avoir
    And Ainsi que la liste des concerts où cette partition a été interprétée
    And Chacun de ces concerts devrait avoir un lien sur son élément constituant amenant à sa page de détails

  Scenario: Détails d’une partition en tant qu’utilisateur non connecté
    Given Je suis un utilisateur non connecté
    When Je tente d’en savoir plus sur une partition
    Then Je devrais être redirigé vers la page de connexion
