/* eslint-disable quotes */

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';

Given(`Je suis un utilisateur connecté`, () => {
  cy.logIn();
});

Given(`Je suis un utilisateur non connecté`, () => {});

When(`Je me rends sur la page qui liste les partitions et j’en choisis une pour avoir davantage de détails`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/scores',
    response: 'fixture:scores/all.json',
  }).as('listScores');

  cy.visit('/scores');
  cy.wait('@listScores');

  cy.route({
    method: 'GET',
    url: '/scores/1',
    response: 'fixture:scores/one.json',
  });
  cy.get('[data-cy=show-score-link]:first').click();
});

When(`Je tente d’en savoir plus sur une partition`, () => {
  cy.server();
  cy.route({
    method: 'GET',
    url: '/scores/1',
    status: 401,
    response: [],
  }).as('getScore');

  cy.visit('/scores/1');
  cy.wait('@getScore');
});

Then(`Je devrais les avoir`, () => {
  cy.get('[data-cy=score-show-title]').should('not.be', 'empty');
});

And(`Ainsi que la liste des concerts où cette partition a été interprétée`, () => {
  cy.get('[data-cy=concerts-score]').should('exist');
});

And(`Chacun de ces concerts devrait avoir un lien sur son élément constituant amenant à sa page de détails`, () => {
  cy.get('[data-cy=concert-score-link]').each(($el, index) => {
    expect($el).to.have.attr('href', `/concerts/${index + 1}`);
  });
});

Then(`Je devrais être redirigé vers la page de connexion`, () => {
  cy.url().should('not.match', /score/);
});
