Feature: Page d'accueil

  Scenario: Ouverture de la page d'accueil
    Given La page est ouverte dans un navigateur
    Then Je devrais voir la page d'accueil
    And qui devrait contenir le formulaire de connexion
    And une barre de titre
    And cette barre doit contenir le bon titre
