export function addPerson(state, person) {
  state.people.push(person);
}

export function removePerson(state, payload) {
  state.people.splice(payload.index, 1);
}

export function setPerson(state, payload) {
  state.people[payload.index] = payload.person;
}

export function defaultPeople(state, people) {
  state.defaultPeople = people;
}

export function reset(state) {
  state.people = [...state.defaultPeople];
}

export function destroy(state) {
  state.people = [];
  state.defaultPeople = [];
}
