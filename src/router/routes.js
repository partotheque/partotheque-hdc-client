const routes = [
  {
    path: '/',
    component: () => import('components/Signin'),
  },
  {
    path: '/search',
    component: () => import('components/Search'),
  },
  {
    path: '/scores',
    component: () => import('components/scores/List.vue'),
  },
  {
    path: '/scores/add',
    component: () => import('components/scores/Add.vue'),
  },
  {
    path: '/scores/:id',
    component: () => import('components/scores/Get.vue'),
  },
  {
    path: '/scores/:id/update',
    component: () => import('components/scores/Update.vue'),
  },
  {
    path: '/people',
    component: () => import('components/people/List.vue'),
  },
  {
    path: '/people/add',
    component: () => import('components/people/Add.vue'),
  },
  {
    path: '/people/:id',
    component: () => import('components/people/Get.vue'),
  },
  {
    path: '/people/:id/update',
    component: () => import('components/people/Update.vue'),
  },
  {
    path: '/concerts',
    component: () => import('components/concerts/List.vue'),
  },
  {
    path: '/concerts/add',
    component: () => import('components/concerts/Add.vue'),
  },
  {
    path: '/concerts/:id',
    component: () => import('components/concerts/Get.vue'),
  },
  {
    path: '/concerts/:id/update',
    component: () => import('components/concerts/Update.vue'),
  },
];

// Always leave this as last one
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue'),
});

export default routes;
