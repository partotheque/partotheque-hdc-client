import Vue from 'vue';
import axios from 'axios';

const API_URL = 'http://localhost:3000';

Vue.prototype.$plainAxios = axios.create({
  baseURL: API_URL,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
  },
});

Vue.prototype.$securedAxios = axios.create({
  baseURL: API_URL,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
  },
});

Vue.prototype.$securedAxios.interceptors.request.use((config) => {
  const method = config.method.toUpperCase();
  if (method !== 'OPTIONS') {
    config.headers = {
      ...config.headers,
      'X-XSRF-TOKEN': localStorage.xsrf,
    };
  }
  return config;
});

Vue.prototype.$securedAxios.interceptors.response.use(null, (error) => {
  if (error.response && error.response.config && error.response.status === 401) {
    delete localStorage.xsrf;
    window.location.replace('/');
    this.$store.commit('signedInModule/changeSignedInState', false);
    return Promise.reject(error);
  }
  return Promise.reject(error);
});
